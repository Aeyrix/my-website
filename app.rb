require './models/post.rb'

class Profile < Sinatra::Base
  set :erb, escape_html: true
  set :show_exceptions, false

  not_found do
    @title = '404'
    erb :notfound
  end

  get '/' do
    @post = Post.latest
    erb :index
  end

  get '/blog' do
    @posts = Post.recent
    @title = 'Blog'
    erb :blog
  end

  get '/post/:slug' do
    @post = Post.get(params[:slug]) or error 404
    @title = @post.title
    erb :post
  end

  get '/projects' do
    @title = 'Projects and Code'
    erb :projects
  end
end
