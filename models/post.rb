require 'RedCloth'

class Post
  attr_accessor :title, :slug, :created_at, :body

  class NotFound < StandardError; end

  def initialize(title, slug, created_at, body)
    @title = title
    @slug = slug
    @created_at = created_at
    @body = body
  end

  def self.update
    Dir['./posts/*.textile'].map do |filename|
      slug = filename[8..-9]
      contents = File.open(filename, 'r').readlines
      title = contents[0][4..-1] # Strip title tag.
      created_at = Date.parse(contents[1])
      body = RedCloth.new(contents[3..-1].join).to_html
      Post.new(title, slug, created_at, body)
    end.compact
  end

  def self.all
    @@all = Hash[update.map { |post| [post.slug, post] }]
  end

  def self.recent
    all.to_a.map{ |slug, post| post }.sort_by{ |post| post.created_at }.reverse
  end

  def self.latest
    recent.first
  end

  def self.get(slug)
    all[slug]
  end
end
